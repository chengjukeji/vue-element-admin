import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    username,
    password
  }
  return request({
    url: '/api-admin/login',
    method: 'post',
    data
  })
}
//注册
export function register(username, password, ensurePassword) {
  const data = {
    username,
    password,
    ensurePassword
  }
  return request({
    url: '/api-admin/register',
    method: 'post',
    data
})
}
export function logout() {
  return request({
    url: '/api-admin/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/api-admin/user/info',
    method: 'get',
    params: { token }
  })
}

