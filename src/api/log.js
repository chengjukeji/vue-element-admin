import request from '@/utils/request'

export function logList(query) {
  return request({
    url: '/api-base/log',
    method: 'get',
    params: query
  })
}
export function removeLog(id) {
  return request({
    url: '/api-base/log/' + id,
    method: 'delete'
  })
}
