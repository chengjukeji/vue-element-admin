import request from '@/utils/request'

export function userList(query) {
  return request({
    url: '/api-admin/user/list',
    method: 'get',
    params: query
  })
}

export function detailUser(id) {
  return request({
    url: '/api-admin/user/' + id,
    method: 'get'

  })
}

export function removeUser(id) {
  return request({
    url: '/api-admin/user',
    method: 'delete',
    params: { id }
  })
}

export function createUser(data) {
  return request({
    url: '/article/create',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: '/article/update',
    method: 'post',
    data
  })
}
