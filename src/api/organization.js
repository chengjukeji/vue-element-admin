import request from '@/utils/request'

export function getMyOrgs() {
  return request({
    url: '/api-admin/organization/getMyOrgs',
    method: 'get'
  })
}
export function saveOrg(organization){
  return request({
  	url: '/api-admin/organization/save',
  	method: 'post',
  	params: organization
  })
}
export function getOrg(orgId){
  return request({
  	url: '/api-admin/organization/' + orgId,
  	method: 'get'
  })
}
export function getOrgMember(orgId){
  return request({
  	url: '/api-admin/organization/getOrgMember',
  	method: 'get',
  	params: {orgId}
  })
}
export function getInviteCode(userId, orgId){
  return request({
  	url: '/api-admin/organization/getInviteCode',
  	method: 'get',
  	params: {userId, orgId}
  })
}
export function joinOrg(inviteCode){
  return request({
  	url: '/api-admin/organization/joinOrg',
  	method: 'get',
  	params: {inviteCode}
  })
}

